#!/bin/bash
# Prepares and starts the Server

sudo systemctl stop pigpiod
./start_stream.sh
sudo systemctl stop pigpiod

sudo node app.js

#!/bin/bash
# Installs Required Dependencies
echo Install required dependencies
sudo apt-get install apache2 node.js npm
sudo apt-get install libjpeg62-turbo-dev
sudo apt-get install cmake
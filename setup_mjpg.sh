#!/bin/bash
# Installs Required Dependencies
echo Setting up mjpg-streamer
mkdir ~/Documents/Projects/mjpg-streamer
cd ~/Documents/Projects/mjpg-streamer
git clone https://github.com/jacksonliam/mjpg-streamer.git ~/Documents/Projects/mjpg-streamer
cd mjpg-streamer-experimental
make clean all
sudo mkdir /opt/mjpg-streamer 
# Only required cause the start_stream.sh in the touch UI project has this path hardcoded
sudo mv * /opt/mjpg-streamer 